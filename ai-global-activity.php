<?php
/**
 * Plugin Name: ai-global-activity
 * Plugin URI: https://git.autistici.org/noblogs/ai-global-activity-plugin
 * Description: Update global activity feed
 * Version: 0.0.24
 * Author: Autistici/Inventati
 * Author URI: https://www.autistici.org/
 * License: MIT
 * License URI: http://opensource.org/licenses/MIT
 */

global $ai_activity_db_version;
$ai_activity_db_version = '1.0';

define('AI_ACTIVITY_TABLE_NAME', 'ai_global_activity');
define('AI_ACTIVITY_PAGE_ARG_NAME', 'actpage');
define('AI_ACTIVITY_DB_VERSION_OPT', 'ai_activity_db_version');

/* Database schema initialization */
function ai_activity_db_setup() {
  global $wpdb;
  global $ai_activity_db_version;

  $table_name = AI_ACTIVITY_TABLE_NAME;
  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $table_name (
    blog_id int(11) NOT NULL,
    post_id int(11) NOT NULL,
    title text,
    author text,
    content text,
    excerpt text,
    url text,
    thumbnail_url text,
    published_at datetime DEFAULT current_timestamp(),
    PRIMARY KEY  (blog_id,post_id),
    KEY published_at (published_at)
  ) $charset_collate;";

  require_once(ABSPATH . "wp-admin/includes/upgrade.php");
  dbDelta($sql);
}

function ai_activity_install() {
  ai_activity_db_setup();
  add_network_option(null, AI_ACTIVITY_DB_VERSION_OPT, $ai_activity_db_version);
}

/* Database schema upgrade check */
function ai_activity_update_db_check() {
  global $ai_activity_db_version;
  if (get_network_option(null, AI_ACTIVITY_DB_VERSION_OPT) != $ai_activity_db_version) {
    ai_activity_db_setup();
    update_network_option(null, AI_ACTIVITY_DB_VERSION_OPT, $ai_activity_db_version);
  }
}

function ai_activity_get_post_image($post) {
    if (preg_match('/<img.+?src=[\'"]([^\'"]+)[\'"].*?>/i', $post->post_content, $matches)) {
        return $matches[1];
    }
    return null;
}

/* Publish post callback */
function ai_activity_publish_post($post_id) {
  global $wpdb;
  $blog_id = get_current_blog_id();

  // Check that the blog is public.
  $blog_archived = get_blog_status($blog_id, 'archived');
  $blog_mature = get_blog_status($blog_id, 'mature');
  $blog_spam = get_blog_status($blog_id, 'spam');
  $blog_deleted = get_blog_status($blog_id, 'deleted');
  if ($blog_archived == '1' || $blog_mature == '1' || $blog_spam == '1' || $blog_deleted == '1') {
    return;
  }

  $post = get_post($post_id, OBJECT, 'display');

  // Only applies to published posts.
  if ($post->post_type != "post" || $post->post_status != "publish" || !empty($post->post_password)) {
    return;
  }

  $post_title = get_the_title($post);
  $post_url = get_permalink($post);
  $post_content = apply_filters('the_content', get_the_content('more...', false, $post));
  $post_excerpt = get_the_excerpt($post);
  $post_author = apply_filters('the_author', $post->post_author);
  $post_date = get_the_date('Y-m-d H:i:s', $post);
  $post_thumbnail_url = get_the_post_thumbnail_url($post, 'thumbnail');
  // Fall back to extracting the image from the post content. This function helpfully
  // returns 'null' if it fails to find an image, which results in a SQL NULL.
  if (!$post_thumbnail_url) {
      $post_thumbnail_url = ai_activity_get_post_image($post);
  }

  // If another entry for the same blog_id/post_id exists,
  // we want to overwrite it (likely someone editing an already
  // published post).
  $wpdb->replace(
		AI_ACTIVITY_TABLE_NAME,
		array(
                      'title' => $post_title,
                      'blog_id' => $blog_id,
                      'post_id' => $post_id,
                      'author' => $post_author,
                      'content' => $post_content,
                      'excerpt' => $post_excerpt,
                      'url' => $post_url,
                      'thumbnail_url' => $post_thumbnail_url,
                      'published_at' => $post_date,
                      )
                );
}

function ai_activity_get_current_page() {
  global $wp_query;
  if (isset($wp_query->query_vars[AI_ACTIVITY_PAGE_ARG_NAME])) {
    return $wp_query->query_vars[AI_ACTIVITY_PAGE_ARG_NAME];
  }
  return 0;
}

function ai_activity_get_previous_page_link() {
  $p = ai_activity_get_current_page();
  if ($p > 0) {
    return "?" . AI_ACTIVITY_PAGE_ARG_NAME . "=" . ($p - 1);
  }
  return "";
}

function ai_activity_get_next_page_link() {
  $p = ai_activity_get_current_page();
  return "?" . AI_ACTIVITY_PAGE_ARG_NAME . "=" . ($p + 1);
}

function ai_activity_get_blog_name($blog_id) {
  $blog_details = get_blog_details($blog_id);
  $blog_name = $blog_details->blogname;
  if (! $blog_name) {
    preg_match('/^https?:\/\/([^.]+)/', get_site_url($blog_id), $matches);
    $blog_name = $matches[1];
  }
  return $blog_name;
}

function ai_activity_get_latest_posts($offset, $limit) {
  global $wpdb;

  $table_name = AI_ACTIVITY_TABLE_NAME;
  $results = $wpdb->get_results($wpdb->prepare(
                                               "
                                               SELECT
                                                 a.blog_id AS blog_id, a.post_id AS post_id, a.title AS title, a.author AS author,
                                                 a.excerpt AS excerpt, a.url AS url, a.thumbnail_url AS thumbnail_url, a.published_at AS published_at
                                               FROM {$table_name} AS a
                                                 JOIN wp_blogs AS b
                                                 ON a.blog_id = b.blog_id
                                               WHERE
                                                 b.public = 1 AND b.spam = 0 AND b.deleted = 0 AND b.mature = 0
                                               ORDER BY published_at DESC
                                               LIMIT %d OFFSET %d
                                               ",
                                               $limit, $offset
                                               ));
  return $results;
}

function ai_activity_display_recent_posts($tmp_number, $tmp_title_characters, $tmp_content_characters, $tmp_title_link = 'no', $tmp_show_avatars = 'yes', $tmp_avatar_size = 16, $posttype = 'post', $show_blog = false, $output = true) {
  $html = '';

  $posts = ai_activity_get_latest_posts($tmp_number * ai_activity_get_current_page(), $tmp_number);

  if ($posts) {
    $default_avatar = get_option('default_avatar');

    foreach ($posts as &$post) {
      $blog_id = $post->blog_id;
      $post_url = $post->url;

      $html .= '<div class="global-activity-post">';

      if ($tmp_title_characters > 0) {
        $html .= '<div class="post-title">';

        if ($tmp_show_avatars == 'yes') {
          $html .= get_avatar($post->author, $tmp_avatar_size, $default_avatar) . ' ';
        }

        $title_short = substr($post->title, 0, $tmp_title_characters);
        if ($tmp_title_link == 'no') {
          $html .= $title_short;
        } else {
          $html .= "<a href=\"" . esc_url($post_url) . "\" >" . esc_html($title_short) . "</a>";
        }

        if ($show_blog) {
          $blog_name = ai_activity_get_blog_name($blog_id);
          $site_url = get_site_url($blog_id);
          $html .= " (<a href=\"" . esc_url($site_url) . "\">" . esc_html($blog_name) . "</a>)";
        }
        $html .= "</div>";
      }

      if ($post->thumbnail_url) {
        $html .= "<img src=\"" . esc_url($post->thumbnail_url) . "\" class=\"alignleft wp-post-image\">";
      }

      $html .= "<div class=\"post-content\">{$post->excerpt}</div>";
      $html .= "</div>";
    }
  }

  $html .= '<div class="global-activity-pagination">';
  $prev_link = ai_activity_get_previous_page_link();
  if ($prev_link) {
    $html .= "<div class=\"nav-previous align-left\"><a href=\"" . esc_url($prev_link) . "\">Newer</a></div>";
  }
  $next_link = ai_activity_get_next_page_link();
  if ($next_link) {
    $html .= "<div class=\"nav-next align-right\"><a href=\"" . esc_url($next_link) . "\">Older</a></div>";
  }
  $html .= '</div>';

  if ($output) {
    echo $html;
  } else {
    return $html;
  }
}

function ai_activity_display_recent_posts_shortcode($attrs) {
  extract(shortcode_atts(array(
                               'number'                => 50,
                               'title_characters'      => 250,
                               'content_characters'    => 0,
                               'title_link'            => 'yes',
                               'show_avatars'          => 'no',
                               'avatar_size'           => 16,
                               'posttype'              => 'post',
                               'show_blog'	           => true
                               ), $attrs));

  return ai_activity_display_recent_posts($number, $title_characters, $content_characters, $title_link, $show_avatars, $avatar_size, $posttype, $show_blog, false);
}

function ai_activity_query_vars($qvars) {
  $qvars[] = AI_ACTIVITY_PAGE_ARG_NAME;
  return $qvars;
}

add_filter('query_vars', 'ai_activity_query_vars');
add_action('publish_post', 'ai_activity_publish_post');
add_action('plugins_loaded', 'ai_activity_update_db_check');
register_activation_hook(__FILE__, 'ai_activity_install');
add_shortcode('globalrecentposts', 'ai_activity_display_recent_posts_shortcode');
